## Module 9: DynamoDB

**1. Create DynamoDB table**

![1](screenshots/1.png)

![2](screenshots/2.png)

![3](screenshots/3.png)

**2. Create a Global Secondary Index (GSI)**

![4](screenshots/4.png)

**3. Choose provisioned capacity mode pricing model**

![5](screenshots/5.png)